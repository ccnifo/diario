---
title: Contact
featured_image: "images/contact.jpg"
omit_header_text: true
description: Et si vous me disiez quelque chose ?
type: page
menu: main

---

Oui, ce carnet est un blog statique, pas interactif par design. Mais je reste joignable (quand je suis connecté au réseau mondial) à l'adresse de courriel :

**caminante(chez)fournials.eu**

et puis je suis aussi sur Wire (@nicolas42) et Signal, alors ne vous privez pas de me donner des nouvelles, vous aussi !
