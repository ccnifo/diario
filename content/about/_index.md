---
title: "A propos"
description: "Un petit mot général et des détails techniques"
#{{< figure src="/images/Victor_Hugo-Hunchback.jpg" title="Illustration from Victor Hugo et son temps (1881)" >}}
---

Un blog tout simple, un peu du fait maison, pour donner des nouvelles de ma belle expérience sud-américaine (et peut-être la suite...).

Merci et bravo aux logiciels et plateformes :  Hugo pour le site statique, Ananke pour ce thème simple et sans dépendance externe, Gitlab pour sa fonction Pages et Framagit pour son hébergement de qualité <3 !


Les mises à jour de ce blog étant peu régulières, sachez qu'ils dispose d'un flux RSS qui permet, à l'aide d'un logiciel approprié, d'être averti de l'arrivée de nouveaux articles.

Le client mail Thunderbird peut gérer des flux RSS ; voici comment le configurer pour ce blog :
- "Nouveau"
- "Compte de flux"
- Terminez la création
- Quand vous êtes sur ce nouveau compte, "Gérer les abonnements"
- "Adresse du flux" : "https://diario.fournials.eu"
- Plus qu'à valider et vous devriez voir apparaitre les articles !

Si vous n'utilisez pas Thunderbird et cherchez un autre logiciel pour cette tâche, à défaut de pouvoir vous en conseiller un, je vous propose de regarder quels sont ceux recensés par Framasoft :
https://framalibre.org/tags/atom

Images : avec Firefox, il vous suffit de faire un clic-droit et "afficher l'image" pour voir une image dans sa taille d'origine. Mes photos (donc toutes les images, sauf mention contraire) sont sous licence **CC-BY-SA 4.0**, ce qui veut dire que vous pouvez les utiliser comme bon vous semble dans tous les endroits qui respectent les licences libres, et donc plus généralement la notion de "bien commun". Les endroits sans barbelés, réels ou virtuels !

