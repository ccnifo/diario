---
date: 2020-02-15
featured_image: "/images/coyhaique-chinchao.jpg"
title: Rat des villes à Coyhaique
---

Me voici donc arrivé dans la grande ville de la région (du haut de ses 50000 habitants), Coyhaique. 
{{< figure src="/images/mapa1.jpg" title="Pour une fois, vous avez droit à une carte" >}}
{{< figure src="/images/coyhaique-chinchao.jpg" title="L'arrivée sur la ville m'a fait penser à Millau. Promis ça y ressemblait plus que sur cette photo" >}}
Images libres, tirées [`de Wikimedia Commons`](https://commons.m.wikimedia.org/wiki/File:Coyhaique_desde_el_Cinchao_-_panor%C3%A1mica.jpg)



Ca faisait longtemps que je n'avais plus été plongé dans une ambiance urbaine, et le moment des retrouvailles a une certaine étrangeté. Mais je me réacclimate vite. C'est que depuis un mois et demi je n'ai plus été en ville, ou du moins pas dans le but de profiter de la ville, plutôt comme de courtes étapes nécessaires sur le trajet.

Alors retrouver un cinéma, une bibliothèque, des librairies, une laverie, peut-être des concerts si j'ai de la chance... Eh bien ça comble un manque qui commençait à s'installer.

{{< figure src="/images/tag2.jpg"  >}}
Aussi, je passe une bonne partie de mes journées à la bibliothèque, quand je ne suis pas au festival de cinéma qui se déroule justement cette semaine ! Il s'appelle "images résistantes" et justement, le retour en ville rappelle qu'en Mars (la rentrée) le mouvement social reprendra. Partout dans la ville on lit des slogans de rébellion, et les banques se sont barricadées façon châteaux forts.

{{< figure src="/images/fecipa.jpg" title="" >}}
A la fois, pour vous donner du contexte, voici les thématiques des 3 premiers court-métrages que j'ai vu : les mères des disparus de la dictature Argentine, la lutte de la famille d'un ado qui a été enlevé par la Police et jamais retrouvé, puis celle de la famille d'un ado tué par un policier qui portait une arme de guerre. Heureusement il y avait un peu plus léger aussi.



{{< figure src="/images/banque.jpg" title="Je ne vous fais pas les traductions, sachez juste que \"paco\" = flic" >}}
{{< figure src="/images/tag1.jpg"  >}}
{{< figure src="/images/animales.jpg" title="Ca ce n'est qu'une enseigne de magasin pour animaux mais... elle vaut le coup d'oeil quand même" >}}

{{< figure src="/images/plaza-arma.jpg" title="La joyeuse Plaza Armas, ses promeneurs et vendeurs de tous poils" >}}

Au quotidien, je suis loin de l'agitation. Je loge via Couchsurfing chez Aldo, un musicien du sud avec qui on boit le maté (tout un rituel qu'ils faudra que j'explique) et qui transforme son petit terrain en camping. J'essaie de donner des petits coups de main, de câliner le chat, d'être moins mauvais aux échecs. Et quand je suis fatigué de tout ça, je me plonge dans les aventures d'Heredia, le Maigret chilien (dont le chat s'appelle d'ailleurs Simenon). Très bon conseil de lecture prodigué par une libraire. Elle n'avait plus rien de Luis Sepulveda, dont le roman "un vieux qui lisait des livres d'amour" a sublimé pas mal d'heures de stop : je recommande vivement, merci Anne-Lise !
{{< figure src="/images/hammac.jpg" title="Peu d'arbres dans le camping, mais j'ai trouvé mon emplacement !" >}}
{{< figure src="/images/pantera.jpg" title="" >}}
{{< figure src="/images/bouquin.jpg" title="Un bouquin, une bière, qué mas?" >}}

{{< figure src="/images/ratata.jpg" title="Depuis que Disney a sorti Ratatouille, c'est plus simple d'expliquer de quoi il s'agit" >}}
{{< figure src="/images/mote.jpg" title="\"Mote con huesillo\" : fait, plus à faire. Du blé, une pêche, beaucoup de sucre et je ne sais quoi... J'ai pas réussi à finir. Et non, je n'ai pas profité de la ville pour aller chez le coiffeur." >}}

Alors j'en suis venu à me demander : serais-je un rat des villes qui joue au rat des champs ? Pour l'instant, je crois que j'apprécie particulièrement l'alternance des deux. Un rat nomade et opportuniste qui manquerait à la fable, en quelque sorte.

Au fait : j'ai mis à jour la page "à propos" avec quelques conseils techniques pour suivre le blog ! N'hésitez pas à me signaler si ce n'est pas clair.
