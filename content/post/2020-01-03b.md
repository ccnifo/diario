---
date: 2020-01-02
featured_image: "/images/lune.jpg"
title: Curiosités célestes
---
Alors bien sûr, dans l'hémisphère sud, on perd notre Étoile Polaire et on cherche la Croix du sud. On a aussi l'eau qui tourbillonne dans le sens opposé de l'hémisphère nord (ne me demandez pas quel sens, ni qui a bien pu s'en apercevoir). Attention, à l'échelle d'un lavabo, il parait que ça ne marche pas...

Mais la dernière surprise en regardant le ciel soir après soir est qu'ici, la Lune qui forme un C est... croissante ! Alors qu'il est connu que la Lune forme un D lorsqu'elle croît et un C lorsqu'elle décroît. Il me manque quelques bases d'astronomie pour apprécier à sa juste mesure ce beau ciel d'été austral.
