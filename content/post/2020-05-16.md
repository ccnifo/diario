---
date: 2020-05-16
featured_image: "/images/spec2-0003.jpg"
title: Il restait des spécialités
---
Bon, si vous avez un peu suivi ce blog je ne peux plus m'en cacher, j'aime bien découvrir un pays aussi par sa nourriture. Et malgré plusieurs articles, j'ai encore plein de choses à vous présenter.

On commence par un grand classique du Chili et de l'Argentine : les **empanadas**. Il s'agit d'une pâte farcie, soit cuite au four soit frite. La farce peut évidemment être faite de milles façons mais le plus courant restent les empanadas de viande ou de fromage. Ça se mange très facilement, c'est un peu le sandwich ou le kebab local. On l'achète par gourmandise ou pour un déjeuner rapide, on le fait chez soi pour varier l'ordinaire. Les chiliens ont l'habitude de composer pour les farcir une mixture appelée **pino** : la viande (ou plus rarement les fruits de mer) y est cuite avec de l'oignon, des oeufs durs, quelques olives.

{{< figure src="/images/spec2-0005.jpg" title="\"Empanada de pino de cochayuyo al horno\"  donc, une algue préparée en pino, dans une empanada cuite au four" >}}
Je ne suis pas le seul français à estimer que le pino est un mélange discutable et que mieux vaudrait parfois une simple viande. Affaire de goût. Je connais des argentins qui en profitent sournoisement pour dire que les chiliens n'y connaissent rien en empanadas, voire en cuisine, mais je ne citerai pas de nom. 



{{< figure src="/images/spec2-0001.jpg" title="" >}}
Hier, nous avons dégusté une spécialité bien chilienne : le **pastel de choclo**. C'est une sorte de délicieux parmentier où la pomme de terre est remplacée par du maïs (qui s'appelle "choclo" au Chili plutôt que "maíz"). En fait de viande, notre hachis était un... pino de tofu ! Eh bien c'était tout de même très bon, comme quoi les préjugés...

Un goût typique de l'Amérique du sud, c'est la confiture de lait, **dulce de leche** en Argentine et **manjar** au Chili. C'est divin pour qui ne craint ni le lait ni le sucre. Et visiblement, très simple à faire pour peu qu'on ait de la vanille. Si quelqu'un connaît un substitut à la vanille, prière de m'en informer de toute urgence.
{{< figure src="/images/spec2-0002.jpg" title="" >}}
On notera que Tryo a écrit une très belle chanson sur l'exil d'un chilien victime du coup d'état, intitulée « El dulce de leche ». La reprise de cette chanson par LEJ est sublime, je ne peux que vous conseiller d'écouter les paroles comme la musique.

{{< figure src="/images/spec2-0003.jpg" title="" >}}
Une nourriture fine on ne peut plus chilienne : les **piñones**. Il s'agit des fruits de l'araucaria : un arbre qui mérite un développement. C'est cet arbre qui m'avait fait forte impression en le découvrant car il produit des paysages où on s'attend à apercevoir des dinosaures (et de fait il est apparu à leur époque). Il marque aussi l'entrée dans le sud du Chili et de l'Argentine. 

{{< figure src="/images/sud.jpg" title="Le lac Aluminé, en Argentine, et ses araucarias. Janvier dernier" >}}
Les piñones sont carrément la base de l'alimentation des Pehuenches, un peuple indigène apparenté aux Mapuches. Tout comme l'alerce, l'araucaria (*araucaria araucana* ou *pin du Chili* ou encore... *Désespoir du singe* !) est un arbre à croissance lente et il peut atteindre le millénaire. Comme l'alerce, son bois possède diverses qualités et il est aujourd'hui classé « en danger d'extinction » et officiellement protégé.

Les piñones se mangent généralement bouillis comme les châtaignes. La coque que vous voyez sur la photo s'ouvre et dévoile une amande au goût fin. J'ai eu la surprise, en lisant un ouvrage fondateur de la permaculture (*Permaculture* de Bill Mollison et David Holmgren) de voir mentionné le pin du Chili dans les plantes qu'on peut cultiver dans un grand terrain. Il est nécessaire d'avoir un peu de patience : les premiers piñones doivent arriver au bout de vingt-cinq ans !

J'ai mentionné les châtaignes : les amateurs savent qu'elles ne sont jamais meilleures que grillées au barbecue (penser à percer leur coque avant...)
{{< figure src="/images/spec2-0007.jpg" title="Et Alvaro nous a offert le privilège de faire un asado, notez les châtaignes qui grillent en bas" >}}
Eh bien les maisons chiliennes possèdent un outil extraordinaire qui permet de griller les châtaignes presque aussi bien : le *tostador*.
{{< figure src="/images/spec2-0004.jpg" title="" >}}
Une double plaque de métal percée de quelques trous pour griller quoi que ce soit sur une cuisinière ou un réchaud. Bien plus polyvalent et transportable qu'un grille pain, l'essayer c'est l'adopter.

Enfin, pour boucler la boucle : il y a un mois et demi je vous montrais qu'on avait semé
{{< figure src="/images/encuisine0022.jpg" title="" >}}
Aujourd'hui nous venons de finir la petite serre que Fran pensait depuis un moment, et nous avons repiqué. 100 % récup : il n'y a que les clous qui sont neufs, et on a juste investi dans un marteau quand on s'est fatigués de les planter avec une pierre. Du coup, le tout a l'aspect et la solidité d'une tente du clownesque [`Condorito`](https://fr.m.wikipedia.org/wiki/Condorito). Tant pis, si ça fonctionne ce sera suffisant !

{{< figure src="/images/spec2-serre.jpg" title="photo de Fran" >}}
{{< figure src="/images/spec2-0006.jpg" title="Juste derrière, les amis kayakistes remarqueront ma vieille corde de sécurité qui finira sa vie ici, comme corde à linge. Certains lui doivent beaucoup, et de manière répétée, mais encore une fois je ne suis pas du genre à dénoncer gratuitement :P" >}}

