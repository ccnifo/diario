---
date: 2020-02-24
featured_image: "/images/voiture.jpg"
title: Quand on suit la bonne route
---

Malgré le fait que j'ai trouvé une belle occasion de découvrir plein de choses à travers un volontariat, je n'ai pas trop envie de partir ! Alors je retarde le moment d'y aller. Et puis sont arrivés au camping Marie, Wattta et Denise. La première est française, les deux autres sont un couple argentin de Terre de Feu qui voyage vers le nord (moins commun que les gens du nord qui vont en Terre de Feu).

Wattta et Denise, récemment mariés, voyagent pour découvrir l'Amérique latine et le monde, et trouver le lieu de vie qui les fera vibrer. Ils sont tous deux jongleurs (malabaristas, en espagnol) et gagnent l'argent de leur voyage en travaillant aux feux rouges des villes où ils s'arrêtent. A Coyhaique, il se trouve que ça ne marche pas : ils repartent donc plus tôt que prévu pour Puerto Aysén, toujours avec Marie.

{{< figure src="/images/mlbr1.jpg" title="" >}}
{{< figure src="/images/mlbr2.jpg" title="" >}}
Alors je demande si je pourrais me glisser, au chausse-pied, dans leur voiture pour aller faire du stop à la sortie de la ville.

{{< figure src="/images/voiture.jpg" title="Et devinez quoi : on est rentrés (de peu)" >}}
Et puis comme ils sont sympa, ils supportent le voyage serré pour 50 kilomètres de plus, jusqu'à ce que nos routes se séparent. Ils me montrent [`la vidéo de leur mariage`](https://anonym.to/?https://www.youtube.com/watch?v=xYNc-OGnGI8) : je n'ai pas résisté à vous mettre un lien (Youtube...) car il y a de quoi réconcilier n'importe qui avec le mariage. Et puis chemin faisant, comme ils sont super sympa ils me demandent s'ils doivent faire leur prochaine demande de Couchsurfing pour trois ou... pour quatre ! J'hésite un peu mais j'ai la liberté de suivre les bonnes ondes, et cette bande-là en dégage un paquet ! Alors c'est parti pour 2 jours à Puerto Aysén. Il se trouve que Marie doit prendre le bateau mardi elle aussi, à Chaitén aussi, alors profitons du moment et partons au dernier instant.

La jongle marche très bien ici, ils sont aux anges et vont rester quelque jours. Un autre jongleur nous a indiqué un super petit camping, on passe de bons moments, on fait un tour de kayak sur le fleuve, on a même droit à un atelier pizza !

{{< figure src="/images/pizza.jpg" title="La pizza en mode camping, ces argentins m'épatent" >}}
{{< figure src="/images/pizza2.jpg" title="" >}}
{{< figure src="/images/puertoaysen.jpg"  >}}
C'est surtout pour des rencontres comme ça que je voyage.

Nos vemos chicos!

