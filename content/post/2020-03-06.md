---
date: 2020-03-06
featured_image: "/images/chiloewwoof0018.jpg"
title: En construction
---
Cela fait donc une dizaine de jours que je suis arrivé au nord de Chiloé.
{{< figure src="/images/chiloemapa.jpg" title="Et hop, la carte pour situer l'île de Chiloé" >}}

J'y ai été accueilli par Timothy et Evelyn, un couple USA-Chili et leurs deux enfants. Ils vivent depuis quelques années dans une grande maison au nord de l'île, avec un grand terrain qui leur permet de vivre de diverses cultures. De temps en temps, ils reçoivent des wwoofers comme moi : c'est là que je dois donner quelques explications.

{{< figure src="/images/chiloewwoof0017.jpg" title="Mais il faut aussi que je vous montre à quoi ressemble une belle soirée vue d'ici" >}}
Je parle donc de volontariat : le fait de fournir une aide sur certaines tâches courantes ou exceptionnelles en échange du gîte et du couvert. Le Wwoof (World-Wide Opportunities on Organic Farms) est un réseau parmi d'autres permettant de mettre en relation des volontaires avec des organisations qui peuvent avoir besoin de main d'oeuvre. En l'occurrence, un réseau orienté agriculture biologique, et celui que j'ai choisi avant de partir, faute d'en connaître d'autres !
Le volontariat peut être l'occasion d'un échange génial entre personnes qui ont des cadres de vie très différents. Ca peut sinon être une occasion de dumping social via l'accomplissement gratuit de tâches ingrates, d'où l'importance de bien lire entre les lignes les annonces. Moi, j'y vois une chance de vivre un peu de la vie de locaux, sans se presser (pas de pression financière puisqu'on a de quoi manger et dormir) et, dans mon idéal, en apprenant des choses, en découvrant des métiers et des styles de vie.

Les annonces qui me paraissent intéressantes et sans entourloupe sont rares, mais là pas de soucis : chez Timothy et Evelyn, on cherche à vivre une vie saine et paisible et on sait accueillir les visiteurs quand on a besoin d'un coup de main. Timothy cultive beaucoup d'ail (c'est une des spécialités de l'ile), mais celui de cette année est déjà récolté.

{{< figure src="/images/chiloewwoof0002.jpg" title="L'ail ici, ça rigole pas : ce ne sont pas mes mains qui ont rétréci. Et bio avec ça." >}}
Alors le grand projet du moment, c'est de construire un kiosque pour qu'Evelyn ait un lieu dédié à la vente de ses produits phyto-thérapeutiques, issus des cultures du jardin (camomille, calendula, miel, aloe vera, concombre, lavande, ...)
{{< figure src="/images/jabones.jpg" title="Que de beaux produits j'ai reçu en cadeau !" >}}

Nous travaillons donc avec Timothy, architecte, maître d'oeuvre et ouvrier improvisé, à monter un petit édifice en bois de 3x3 mètres, qui devra notamment résister aux vents violents de ce coin de paradis près de l'océan Pacifique. Les constructions en bois sont ici la norme, la pierre l'exception.

{{< figure src="/images/chiloewwoof0020.jpg" title="Des fondations à plat sur un terain pentu, premier défi" >}}
{{< figure src="/images/chiloewwoof0018.jpg" title="\"Good enough!\"" >}}
{{< figure src="/images/chiloewwoof0015.jpg" title="Arrivés là, on est déjà contents" >}}
{{< figure src="/images/chiloewwoof0009.jpg" title="Premier mur en cours" >}}
{{< figure src="/images/chiloewwoof0010.jpg" title="1 foot = 12 inches. On travaille avec le système métrique, dieu merci, mais étonnamment les poutres sont découpées en dimensions USA" >}}
{{< figure src="/images/chiloewwoof0008.jpg" title="" >}}
{{< figure src="/images/chiloewwoof0007.jpg" title="" >}}
{{< figure src="/images/chiloewwoof0006.jpg" title="" >}}
{{< figure src="/images/chiloewwoof0005.jpg" title="" >}}
{{< figure src="/images/chiloewwoof0003.jpg" title="" >}}
Je dois rester encore quelques jours, avec l'espoir que nous ayons terminé de poser le toit d'ici-là !

De temps à autre on fait aussi de petites récoltes, en particulier du mesclun que Tim vend en sachets prêts à l'emploi à des restaurants de la ville voisine.

{{< figure src="/images/chiloewwoof0014.jpg" title="" >}}
{{< figure src="/images/chiloewwoof0013.jpg" title="" >}}
{{< figure src="/images/chiloewwoof0012.jpg" title="" >}}
{{< figure src="/images/chiloewwoof0011.jpg" title="" >}}
Et qui dit volontariat dit aussi temps libre (en théorie, le volontaire ne doit pas dépasser 20 ou 25 heures par semaine) : du temps pour lire, entre autres. En plus chez Timothy et Evelyn, il y a un paquet de livres, mais celui qui a retenu mon attention, c'est le classique "Jurassic Park".

{{< figure src="/images/chiloewwoof0004.jpg" title="Salon de lecture avec vue sur l'estuaire (surexposé ici...)" >}}
L'idée de base, une île parc d'attraction où l'ingénierie génétique appuyée par un millionnaire excentrique a réussi à recréer des dinosaures, est géniale et participe à en faire un récit captivant et plein d'inattendu. Bien plus étoffé que le film de Spielberg. Malheureusement, le récit est estropié par une foule de situations improbables et d'incohérences.

Mais ce qui m'aura surtout marqué de cette lecture, c'est la critique acide des scientifiques comme de la science elle-même. Le mathématicien désabusé Ian Malcolm la décrit comme un pouvoir hérité : on se hisse sur des épaules de géant sans passer par un parcours initiatique qui nous rendrait méritants mais surtout conscients et précautionneux. La critique des ingénieurs et chercheurs est savoureuse, je vous en cite un bout (j'ai pas le talent de traduire mais faites pas la tête, ce sont des phrases simples) :
> "He's all right. He's an engineer. Wu's the same. They're both technicians. They don't have intelligence. They have what I call ‘thintelligence.’ They see the immediate situation. They think narrowly and they call it ‘being focused.’ They don't see the surround. They don't see the consequences."

Et bim. J'en prends aussi pour mon grade mais j'aime la distance et la perspective que donne ce point de vue.


Le temps libre me sert aussi pour aller voir autour ce qui se passe.

La vile voisine, Ancud, était parait-il un joyau au bord l'océan. Mais en 1960, le plus fort séisme enregistré à ce jour a dévasté une bonne partie du Chili. Le tsunami qui l'a suivi a achevé la destruction. Aujourd'hui, ce sont donc les cris des mouettes et l'odeur iodée du front de mer qui font, pour ce que j'en ai vu, l'attrait d'Ancud.

{{< figure src="/images/chiloewwoof0019.jpg" title="" >}}
Plus au sud, Castro a été un peu moins touchée et conserve une partie de ses maisons sur pilotis ainsi qu'un centre animé. Trop animé même, quand arrive un évangéliste illuminé qui hurle dans un micro, au grand dam des passants qui profitaient d'un bel après-midi d'été.

{{< figure src="/images/chiloewwoof0016.jpg" title="Quelques maisons sur pilotis" >}}
L'été, c'est redevenu une réalité en remontant depuis Coyhaique jusqu'ici : les herbes sèches, le goudron chaud et ses odeurs, le Soleil qui tape. Des choses apparemment banales que je n'avais pourtant plus rencontrées depuis un mois, quand j'ai pris le premier ferry pour la Patagonie chilienne.


{{< figure src="/images/chiloewwoof0001.jpg" title="Et encore une belle soirée sur l'île..." >}}

