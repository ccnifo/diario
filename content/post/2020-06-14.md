---
date: 2020-06-14
featured_image: "/images/sureno0011.jpg"
title: Le sud dans la peau ?
---
Et voilà, une page s'est tournée pour moi. J'écris depuis une chambre d'auberge avec une vue dégagée et assez paisible sur des immeubles et au loin quelques montagnes. Le ciel est sans nuages, juste un léger voile gris comme les citadins connaissent. La nuit sera sans étoiles. Les bruits de la rue arrivent jusque dans ma chambre par la fenêtre, profitant des nombreux espaces dans la vieille menuiserie. Le hammac est dans le sac, je me remets à dormir dans un lit. Je suis arrivé à Santiago.
{{< figure src="/images/sureno0002.jpg" title="Un dernier regard sur la péninsule Comau" >}}

Je n'ai pas dit grand chose de mes dernières semaines à Ayacara. Soit parce qu'il n'y avait pas grand chose d'exceptionnel, soit parce que j'en profitais à fond. Sans doute les deux. En tout cas maintenant que je suis parti il faudra que je me rattrape : j'ai passé plusieurs mois dans un lieu mondialement reconnu pour son écosystème unique et menacé, je me suis émerveillé devant les oiseaux, les dauphins,  les arbres et autres plantes, ça vaudra bien que j'en dise un mot. Mais en ce moment, au milieu de cette sorte d'île qu'est une ville confinée, c'est le départ que je vais raconter.

Cela fait plus de trois mois que j'ai passés avec Franchesca : on avait fait les touristes ensemble à Chiloé, à Puerto Montt, puis en Argentine. Jusqu'à se faire rattraper par la pandémie qui nous a fait rentrer en catastrophe au Chili avant que la frontière ne se ferme (pour moi du moins). Après quelques jours de plus à Puerto Montt, à profiter des bonnes vibrations des amies de Fran, nous sommes partis chez elle, à Ayacara. Il y a deux mois et demi. 

Ayacara, c'est un endroit singulier : pas de liaison routière au reste du pays. Pas de supermarché, pas de magasin de vêtements, pas de bar. Pas de pompe à essence avec pistolet et compteur, le plein se fait avec des bidons. Pas d'étrangers en ce moment, à part évidemment un français dont personne ne savait trop ce qu'il faisait là. L'eau du robinet vient directement par un tuyau du ruisseau au-dessus, elle est gratuite et arrive à pleine pression et sans avoir vu un filtre : on la boit donc bouillie quoiqu'elle semble plutôt propre hormis les jours de fortes pluies. L'électricité est générée par une petite centrale locale : je pense que personne sur la péninsule n'a eu l'idée saugrenue de l'utiliser pour remplacer le bois et le gaz pour le chauffage, l'eau chaude ou la cuisson.  Si on a besoin de quelque outil spécial, il faut y penser quand on est à Puerto Montt : jusqu'à preuve du contraire, même Amazon ne vient pas livrer sur la péninsule Comau. Idem pour les aliments sortant de l'ordinaire, les livres, etc. Il n'y a pas un seul distributeur de billets, ni un commerçant qui accepte les cartes internationales : il faut avoir un compte chez BancoEstado, seul réseau de paiement et de dépôt/retrait d'argent disponible au fond du Chili. Il y aurait sans doute encore à dire mais... après tant de temps, tout cela est devenu naturel, et il me faut me concentrer pour trouver les singularités de ce lieu.

Deux mois et demi à Ayacara, et quatre mois de mon voyage (qui n'en compte pas six) passés dans la région de *Los Lagos*. Dont plus de trois mois avec Fran, la santiaguine devenue fille du sud. Los Lagos, c'est la région de Chiloé, de Futaleufú, de Puerto Montt, la porte et le début de la Patagonie chilienne. Également la région où j'ai vu les paysages les plus beaux : les environs d'Hornopirén et le parc Pumalín en tête, mais pas seulement.

{{< figure src="/images/sureno0006.jpg" title="Los Lagos, grande comme Midi-Pyrénées, avec en vert le début de la Patagonie" >}}
{{< figure src="/images/cp2.jpg" title="Río Blanco d'Hornopirén" >}}
{{< figure src="/images/cascadaalta.jpg" title="Dans le parc Pumalín" >}}
{{< figure src="/images/pet2.jpg" title="Un rapide sublime du Río Petrohué" >}}

Alors forcément, quand je pense au Chili, j'ai un biais, une orientation au sud. Hier, en voyant Santiago s'approcher à travers les vitres du bus, j'ai eu l'impression de revoir une image connue, qui date de décembre ou janvier dernier, mais j'ai surtout eu une impression de dépaysement extraordinaire. Pas besoin d'aller de l'extrême nord désertique à l'extrême sud polaire pour avoir un contraste saisissant : demi-heure de coucou à hélice jusqu'à Puerto Montt puis 1032 kilomètres de bus de nuit pour Santiago suffisent à ne plus reconnaître le pays où on se trouve.

À Santiago, les gens portent des masques pour de vrai et rentrent au compte-goutte dans les commerces. En entrant dans un supermarché, j'ai été vraiment surpris : comment cet environnement nous est-il devenu naturel ? Vous avez remarqué cette lumière blanche et forte, ces couleurs dans tous les sens, ces marques qui saturent la vue tellement elles sont nombreuses ? Vous avez senti la bizarrerie de cette abondance sous plastique qui demande juste à tendre le bras ? Ceux qui avaient lu mon billet sur la télévision reconnaîtront peut-être la même idée : entrer dans un supermarché après 11 semaines de sevrage, c'est faire une expérience surnaturelle qui vaut le coup d'être vécue. Je recommande réellement.
{{< figure src="/images/sureno0007.jpg" title="Une épicerie d'Ayacara. Basique, fonctionnelle, simple. La question devrait être posée dans ce sens-là : qu'est ce que je vois en plus dans un hyper, et à quoi ça sert ?" >}}

Hier, je me suis surpris à penser « Oh, un jour de Soleil. Profites-en pour sortir, ça changera peut-être demain ! ». Oui mais en fait, à Santiago ce sont les jours de pluie qui sont rares.
{{< figure src="/images/sureno0011.jpg" title="À Ayacara, ceci est une météo qui appelle à sortir profiter. À raison, non ?" >}}

Accosté par un chauffeur de taxi au sortir du bus, j'explique que je vais à pied. « Avec ce froid ? » me lance t-il, l'air surpris. Oui, il est 9 h du matin et en effet il doit faire moins de dix degrés. Mais après les nuits glaciales où le feu s'éteignait à Ayacara, les journées à pédaler contre le vent du sud qui apporte le Soleil et le froid, marcher par une matinée fraîche et ensoleillée n'est rien de plus qu'agréable.

Ça fait toujours bizarre de partir, de s'arracher. D'autant plus quand l'endroit devenu familier est près du bout du monde, ce qui diminue les chances d'y retourner pour une pensée rationnelle. D'autant plus quand on a vécu trois mois avec une amie qui reste. D'autant plus quand le chemin retour passe par des endroits familiers qui se sont désertés. Santiago est confinée, râpé pour revoir Blaise et Magda (par chance, je peux revoir mon ami Gaël qui habite à deux pas de l'auberge). Vale, Deya et Javi sont parties à droite à gauche pour diverses raisons : Puerto Montt n'a guère changé d'aspect mais n'a plus la même saveur. Au passage, vous pouvez écouter Deya, la nutritionniste, dans [`cette émission de Radio Universidad de Concepción, à la date du 6 juin`](https://www.radioudec.cl/2020/02/05/alimenta-tu-vida/). Même si vous ne parlez pas espagnol, je vous recommande vivement d'écouter l'émission à 11 minutes pour entendre la superbe reprise de Violetta Parra qu'ont enregistré Vale et Deya ! Nous avons entendu cette chanson plus d'une fois dans la maison de Vale, c'est un beau souvenir en plus d'être un grand classique de la chanson chilienne.
{{< figure src="/images/sureno0001.jpg" title="Puerto Montt vendredi, une journée contrastée" >}}

Un clin d'oeil : à Puerto Montt, une rencontre sympathique fut celle d'une vénézuélienne qui y souffrait du froid ; une pensée pour une chère vénézuélienne qui souffre des hivers lyonnais !

{{< figure src="/images/sureno0003.jpg" title="" >}}
Bref, je suis monté dans un avion neuf places qui fait un boucan terrible pour commencer les 12 000 kilomètres qui me ramèneront  en France, où la suite se dessine. Mercredi je devrais être revenu au pays de [`Pépé le putois`](https://fr.wikipedia.org/wiki/P%C3%A9p%C3%A9_le_putois) (eh oui, cet odieux personnage de cartoon qui chez nous a un accent italien est... une caricature de français !). 

J'ai beau avoir une certaine habitude des départs, ce ne sont pas toujours des moments plaisants.
Mais j'emporte avec moi un peu du sud du monde, et je vous prends à témoin : si d'ici quelques mois je suis redevenu pas plus que celui que vous connaissiez avant, je serai un sacré bourricot. Je vous prie de me le rappeler.


{{< figure src="/images/franchute.jpg" title="\"monumento al franchute\", Ayacara, photo de Fran. Statue temporaire qui n'y est déjà plus : mieux que les statues de bronze qui nous font oublier que rien n'est éternel. S'il y avait quelque chose d'important ici, c'était de profiter du coucher de Soleil">}}

