---
date: 2020-01-09
featured_image: "/images/hiver.jpg"
title: Bref hiver
---

2 jours de sale temps (froid, vent, pluie) nous auront donné du fil a retordre, mais nous auront conduit à prendre l'option de 2 nuits au sec dans une auberge conviviale de Pucon. Ce n'était pas de trop, car les conditions hivernales en janvier au Chili, on n'a pas signé pour ça.
Néanmoins le Rio Truful Truful hier et le Trancura aujourd'hui valaient bien la peine de se donner du mal à enfiler la combinaison mouillée ! On n'aura juste pas eu le plaisir de voir le paysage volcanique à priori très beau. Sur le Trancura, la quantité d'eau sous le bateau est vraiment impressionnante, pour ne pas dire effrayante.


{{< figure src="/images/marinan.jpg" title="LE rapide sur le Trancura. Monstrueux. Comme dirait Blaise, ici on comprend qu'on n'est pas des kayakistes de classe V. Et donc on passe à pied." >}}
{{< figure src="/images/marche.jpg" title="Pause déjeuner au marché" >}}
{{< figure src="/images/pudu.jpg" title="Pudu, les meilleures glaces du continent. Encore mieux que les glaces argentines paraît-il. Magda est argentine et n'approuve pas." >}}
{{< figure src="/images/carmenere.jpg" title="On finit la journée sur un petit rouge chilien, un Carménère (cépage peu répandu en France si je ne m'abuse) et un \"gratine de weon\" (raviolis au four à la Blaise), on est bien..." >}}

