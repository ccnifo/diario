---
title: "Un carnet"
featured_image: '/images/bandeau.jpg'
description: "Bon baisers du Chili"
---

Des pensées, des images, mises à jour au gré de l'inspiration et des connexions au Réseau des réseaux.
